package moviedatabase;

/**
 *
 * @author Christopher McCormick
 */
public class Media {
    
    //member variables
    private String mTitle;
    private Format mFormat;
    private boolean mCheckedOut;
    private String mCheckedOutLocation;
    private Genre mGenre;
    private boolean mTelevisionSeries;
    
    enum Format
    {
        VHS, DVD, Bluray, Digital, Other;
    }
    enum Genre
    {
        Action, Comedy, Drama, Holiday, HomeVideo, Kids, Other;
    }
    
    
    //Constructor for creating a new Media file
    public Media(String title, Format format, Genre genre, boolean television)
    {
        
        mTitle = title.replaceAll(" ", "_");
        mFormat = format;
        mGenre = genre;
        mTelevisionSeries = television;
        mCheckedOut = false;
        mCheckedOutLocation = "N/A";
    }
    
    //Constructor for loading existing Media files
    public Media(String title, Format format, Genre genre, boolean television, boolean checkedOut, String checkedOutLocation)
    {
        mTitle = title;
        mFormat = format;
        mGenre = genre;
        mTelevisionSeries = television;
        mCheckedOut = checkedOut;
        mCheckedOutLocation = checkedOutLocation;
    }
    
    //Constructor for null filled media for use in search results
    public Media()
    {
        mTitle = null;
        mFormat = null;
        mGenre = null;
        mTelevisionSeries = false;
        mCheckedOut = false;
        mCheckedOutLocation = null;
    }
    
    
    //getters
    public String get_title()
    {
        return mTitle;
    }
    
    public String get_title_string()
    {
        return mTitle.replaceAll("_", " ");
    }
    
    public String get_format()
    {
        return mFormat.name();
    }
    
    public String get_genre()
    {
        return mGenre.name();
    }    
    
    public boolean get_televisionSeries()
    {
        return mTelevisionSeries;
    }
    
    public String get_televisionSeries_string()
    {
        if(mTelevisionSeries)
            return "True";
        else
            return "False";
    }
    
    public boolean get_checkedOut()
    {
        return mCheckedOut;
    }
    
    public String get_checkedOut_string()
    {
        if(mCheckedOut)
            return "True";
        else
            return "False";
    }
    
    public String get_checkedOutLocation()
    {
        return mCheckedOutLocation;
    }
    
    public String get_checkedOutLocation_string()
    {
        return mCheckedOutLocation.replaceAll("_", " ");
    }
    
    
    //setters
    public void set_checkedOut(boolean b)
    {
        mCheckedOut = b;
    }
    
    public void set_checkedOutLocation(String s)
    {
        mCheckedOutLocation = s.replaceAll(" ", "_");
    }
}
