package moviedatabase;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import moviedatabase.Media.Format;
import moviedatabase.Media.Genre;

/**
 *
 * @author Christopher McCormick
 */


/*
------------------------
  Future Content Ideas
------------------------
 - Edit Media function
    - export media again at the end of the function
 - Userdata backup in case of errors
    - create a backup at start up and save so many before deleting old?
    - create a backup and keep all?
    - give users the option to backup manually?
    - give users the option to backup manually and autobackup periodically?
 - User Favorites
    - create profiles and allow users to save their favorite media
 - Advanced Filter Search
    - filter by genre, format, keyword
 - Restore Backup GUI
*/

public class MovieDatabase extends JPanel implements ActionListener {

    private ArrayList<Media> 
            database = new ArrayList<>(),
            results_genre = new ArrayList<>();
    private String 
            version = "Version 1.0.0";
    private String[] formatOptions = {"DVD", "Blu-ray", "Digital", "VHS", "Other"},
            genreOptions = {"Action", "Comedy", "Drama", "Holiday", "Home Video", "Kids", "Other"},
            televisionSeriesOptions = {"True", "False"},
            searchKeywordOptions = {"          "};
    private boolean
            previousSearchKeyword = false,
            previousSearchGenre = false;
    private JButton 
            button01 = new JButton("Search by Keyword"), 
            button02 = new JButton("Search by Genre"), 
            button03 = new JButton("Advanced Search"), 
            button04 = new JButton("Add Media"), 
            button05 = new JButton("Search"), //by keyword
            button06 = new JButton("Main Menu"),
            button07 = new JButton("Select"), //keyword select
            button08 = new JButton("Check In"),
            button09 = new JButton("Check Out"), //open menu
            button10 = new JButton("Check Out"), //actually check out
            button11 = new JButton("Delete"), //open confirmation
            button12 = new JButton("Delete"), //actually delete
            button13 = new JButton("Cancel"),
            button14 = new JButton("Submit"),
            button15 = new JButton("Backup"),
            button16 = new JButton("Search"), //by genre
            button17 = new JButton("Select"), //genre select
            button18 = new JButton("Return to Search");
    private JLabel 
            label01 = new JLabel("Welcome to Movie Database", JLabel.CENTER), 
            label02 = new JLabel(version, JLabel.CENTER), 
            label03 = new JLabel("Search by Keyword", JLabel.CENTER),
            label04 = new JLabel("Check Out Location:", JLabel.CENTER),
            label05 = new JLabel("Are you sure you want to delete this?"),
            label06 = new JLabel("Add Media"),
            label07 = new JLabel("Title:"),
            label08 = new JLabel("Genre:"),
            label09 = new JLabel("Format:"),
            label10 = new JLabel("TV Series:"),
            label11 = new JLabel("Search by Genre");
    private JTextField
            textField01 = new JTextField(),
            textField02 = new JTextField(),
            textField03 = new JTextField();
    private JEditorPane
            editorPane01 = new JEditorPane();
    private JComboBox 
            formatList,
            genreList,
            televisionSeriesList,
            genreList2,
            searchKeywordList = new JComboBox(),
            searchGenreList = new JComboBox();  
    private DefaultComboBoxModel 
            searchKeywordModel = (DefaultComboBoxModel) searchKeywordList.getModel(),
            searchGenreModel = (DefaultComboBoxModel) searchGenreList.getModel();
    private Font
            font01 = new Font("Helvetica", Font.BOLD, 24);
    private GridBagConstraints constraints = new GridBagConstraints();
    private static JFrame frame = new JFrame();
    private Media[] results_keyword = {new Media(), new Media(), new Media()};
    private Media selection;
    private File 
            mdbDirectory = new File(System.getProperty("user.home").concat("/.moviedatabase")),
            userdataDirectory = new File(mdbDirectory.toString().concat("/userdata")),
            backupDirectory = new File(mdbDirectory.toString().concat("/backup")),
            userdataFile = new File(userdataDirectory.toString().concat("/database.txt"));
    
    
    @SuppressWarnings("unchecked")
    public MovieDatabase(){  
        
        //initialize JComboBoxes
        this.genreList = new JComboBox(genreOptions);
        this.genreList2 = new JComboBox(genreOptions);
        this.formatList = new JComboBox(formatOptions);
        this.televisionSeriesList = new JComboBox(televisionSeriesOptions);
        
        //add Action Listeners
        button01.addActionListener(this);
        button02.addActionListener(this);
        button03.addActionListener(this);
        button04.addActionListener(this);
        button05.addActionListener(this);
        button06.addActionListener(this);
        button07.addActionListener(this);
        button08.addActionListener(this);
        button09.addActionListener(this);
        button10.addActionListener(this);
        button11.addActionListener(this);
        button12.addActionListener(this);
        button13.addActionListener(this);
        button14.addActionListener(this);
        button15.addActionListener(this);
        button16.addActionListener(this);
        button17.addActionListener(this);
        button18.addActionListener(this);
        textField01.addActionListener(this);
        textField02.addActionListener(this);
        textField03.addActionListener(this);
        formatList.addActionListener(this);
        genreList.addActionListener(this);
        televisionSeriesList.addActionListener(this);
        genreList2.addActionListener(this);

        //window layouts and constraints
        setLayout(new GridBagLayout());
        constraints.insets = new Insets(15, 5, 15, 5); //top, left, bottom, right
        
        //make sure directories exists 
        if(!mdbDirectory.exists())
            mdbDirectory.mkdir();
        if(!userdataDirectory.exists())
            userdataDirectory.mkdir();
        if(!backupDirectory.exists())
            backupDirectory.mkdir();
        
        
        //import media files and create menu
        import_media(userdataFile.toString());
        open_menu_main();
    }
    
    public void actionPerformed(ActionEvent ae) {
        Object eventSource = ae.getSource();
        
        if(eventSource == button01) {
            open_menu_search_keyword();
        }
        if(eventSource == button02) {
            open_menu_search_genre();
        }
        if(eventSource == button03) {
            user_notification("Advanced Search coming soon!");
        }
        if(eventSource == button04) {
            open_menu_add_media();
        }
        if(eventSource == button05 || eventSource == textField01) {
            search_keyword();
        }
        if(eventSource == button06) {
            open_menu_main();
        }
        if(eventSource == button07 || eventSource == button13) {            
            
            //if(( searchKeywordList.getSelectedIndex() < 0 && searchKeywordList.getSelectedIndex() >= results_keyword.length ))
            if(searchKeywordList.getSelectedIndex() == -1)
                return;
            if(eventSource == button07)
                selection = results_keyword[searchKeywordList.getSelectedIndex()];
            
            open_menu_display_media(selection);
        }
        if(eventSource == button08) {
            check_in(selection);
        }
        if(eventSource == button09) {
            open_menu_check_out(selection);
        }
        if(eventSource == button10) {
            check_out(selection, textField02.getText());
        }
        if(eventSource == button11) {
            open_menu_delete(selection);
        }
        if(eventSource == button12) {
            delete(selection);
        }
        if(eventSource == button14) {
            add_media();
        }
        if(eventSource == button15) {
            export_media(backupDirectory.toString() + "/" + java.time.LocalDate.now() + "_" + java.time.LocalTime.now() + ".txt");
            user_notification("Backup created successfully.");
            //todo: Actually verify backup creation is successful
        }
        if(eventSource == button16) {
            search_genre((String)genreList2.getSelectedItem());
        }
        if(eventSource == button17) {
            if(searchGenreList.getSelectedIndex() == -1)
                return;
            
            for (Media media : database)
            {
                if(searchGenreList.getSelectedItem().equals(media.get_title_string()))
                {
                    selection = media;
                    break;
                }
            }
            open_menu_display_media(selection);            
        }
        if(eventSource == button18) {
            open_menu_search_previous();
        }
    }
    
    //------------------------------------------------------------------------//
    
    public static void main(String[] args) {
        MovieDatabase database_main = new MovieDatabase();
        frame.setTitle("Movie Database");
        frame.setSize(800,600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.add(database_main);
        
        
    }
    
    private void import_media(String input)
    {
        
        if(!userdataFile.exists())
            return;
        
        try {
            Scanner scanner = new Scanner(new File(input));
            
            while(scanner.hasNextLine())
            {
                String line = scanner.nextLine();
                
                String[] mediaArray = line.split(" ");
                
                //Instantiate variables needed for
                Format format;
                Genre genre;
                boolean television;
                boolean checkedOut;
                
                //find format
                switch(mediaArray[1])
                {
                    case "Bluray":
                        format = Format.Bluray;
                        break;
                    case "DVD":
                        format = Format.DVD;
                        break;
                    case "Digital":
                        format = Format.Digital;
                        break;
                    case "VHS":
                        format = Format.VHS;
                        break;
                    default:
                        format = Format.Other;
                        break;
                }
                
                //find genre
                switch(mediaArray[2])
                {
                    case "Action":
                        genre = Genre.Action;
                        break;
                    case "Comedy":
                        genre = Genre.Comedy;
                        break;
                    case "Drama":
                        genre = Genre.Drama;
                        break;
                    case "Holidy":
                        genre = Genre.Holiday;
                        break;
                    case "HomeVideo":
                        genre = Genre.HomeVideo;
                        break;
                    case "Kids":
                        genre = Genre.Kids;
                        break;
                    default:
                        genre = Genre.Other;
                        break;
                }
                
                //find if television series
                if(mediaArray[3].equalsIgnoreCase("true"))
                    television = true;
                else
                    television = false;
                
                //find if checked out
                if(mediaArray[4].equalsIgnoreCase("true"))
                    checkedOut = true;
                else
                    checkedOut = false;
                    
                database.add(new Media(mediaArray[0], format, genre, television, checkedOut, mediaArray[5]));
                
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MovieDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    private void export_media(String output)
    {
        try {
            
            BufferedWriter bw = new BufferedWriter(new FileWriter(output));

            for(Media m : database)
            {
                bw.append(m.get_title() + " " + m.get_format() + " " + m.get_genre() + " " + m.get_televisionSeries() + " " + m.get_checkedOut() + " " + m.get_checkedOutLocation());
                bw.newLine();
            }
            
            bw.close();
            
        } catch (IOException ex) {
            Logger.getLogger(MovieDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    //------------------------------------------------------------------------//
    //                              Menus                                     //
    //------------------------------------------------------------------------//

    
    private void open_menu_main()
    {
        removeAll();
        
        //button01
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.NONE;
        add(button01, constraints);
        
        //button02
        constraints.gridx = 1;
        constraints.gridy = 0;
        add(button02, constraints);
        
        //button03
        constraints.gridx = 2;
        constraints.gridy = 0;
        add(button03, constraints);
        
        //label01
        label01.setVerticalTextPosition(JLabel.CENTER);
        label01.setHorizontalTextPosition(JLabel.CENTER);
        label01.setFont(font01);
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 3;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(label01, constraints);
        
        //button04
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(button04, constraints);
        
        //button15
        constraints.gridx = 2;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(button15, constraints);

        //label02
        label02.setVerticalTextPosition(JLabel.CENTER);
        label02.setHorizontalTextPosition(JLabel.RIGHT);
        constraints.gridx = 2;
        constraints.gridy = 4;
        constraints.gridwidth = 1;
        add(label02, constraints);
        
        validate();
        repaint();
    }
    
    private void open_menu_search_keyword()
    {
        previousSearchKeyword = true;
        previousSearchGenre = false;
        
        removeAll();
        
        //label03
        label03.setVerticalTextPosition(JLabel.CENTER);
        label03.setHorizontalTextPosition(JLabel.CENTER);
        label03.setFont(font01);
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(label03, constraints);
        
        //textField01
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(textField01, constraints);
        
        //button05
        constraints.gridx = 2;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.NONE;
        add(button05, constraints);
        
        //button06
        constraints.gridx = 0;
        constraints.gridy = 4;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.NONE;
        add(button06, constraints);   
        
        //searchKeywordList
//        searchKeywordList.setSelectedIndex(0);
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(searchKeywordList, constraints);
        //searchKeywordList
//        searchKeywordList.setSelectedIndex(0);
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(searchKeywordList, constraints);
        //button07
        constraints.gridx = 2;
        constraints.gridy = 3;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.NONE;
        add(button07, constraints); 
                
        //button06
        constraints.gridx = 0;
        constraints.gridy = 5;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.NONE;
        add(button06, constraints);
        
        validate();
        repaint();
    }
    
    private void open_menu_display_media(Media media)
    {
        removeAll();
        
        editorPane01.setContentType("text/html");
        editorPane01.setText("<h1>" + media.get_title_string() + "</h1><b>"
            + "Genre: </b>" + media.get_genre() + "<br><b>"
            + "Format: </b>" + media.get_format() + "<br><b>"
            + "Television Series: </b>" + media.get_televisionSeries_string() + "<br><br><b>"
            + "Checked Out: </b>" + media.get_checkedOut_string() + "<br><b>"
            + "Checked Out Location: </b>" + media.get_checkedOutLocation_string()
            );
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 3;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(editorPane01, constraints);
                
        //button08/09
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        if(media.get_checkedOut())
            add(button08, constraints);
        else
            add(button09, constraints);
            
        
        //button11
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(button11, constraints); 
        
        //button18
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(button18, constraints); 
        
        //button06
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(button06, constraints);
        
        validate();
        repaint();
    }
    
    private void open_menu_check_out(Media m)
    {
        removeAll();
        
        //label04
        label04.setVerticalTextPosition(JLabel.CENTER);
        label04.setHorizontalTextPosition(JLabel.CENTER);
        label04.setFont(font01);
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(label04, constraints);
        
        //textField02
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(textField02, constraints);
        
        //button10
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(button10, constraints);
        
        //button13
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(button13, constraints);
        
        validate();
        repaint();
    }
    
    private void open_menu_delete(Media m)
    {
        removeAll();
        
        //label05
        label05.setVerticalTextPosition(JLabel.CENTER);
        label05.setHorizontalTextPosition(JLabel.CENTER);
        label05.setFont(font01);
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(label05, constraints);
        
        //button12
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(button12, constraints);
        
        //button13
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(button13, constraints);
        
        validate();
        repaint();
    }
    
    private void open_menu_add_media()
    {
        removeAll();
        
        //label06
        label06.setVerticalTextPosition(JLabel.CENTER);
        label06.setHorizontalTextPosition(JLabel.CENTER);
        label06.setFont(font01);
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 3;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(label06, constraints);
        
        //label07
        label07.setVerticalTextPosition(JLabel.CENTER);
        label07.setHorizontalTextPosition(JLabel.CENTER);
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(label07, constraints);
        
        //textField03
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        textField03.setText("");
        add(textField03, constraints);
        
        //label08
        label08.setVerticalTextPosition(JLabel.CENTER);
        label08.setHorizontalTextPosition(JLabel.CENTER);
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(label08, constraints);
        
        //formatList
        formatList.setSelectedIndex(0);
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(formatList, constraints);
        
        //label09
        label09.setVerticalTextPosition(JLabel.CENTER);
        label09.setHorizontalTextPosition(JLabel.CENTER);
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(label09, constraints);
        
        //genreList
        genreList.setSelectedIndex(0);
        constraints.gridx = 1;
        constraints.gridy = 3;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(genreList, constraints);
        
        //label10
        label10.setVerticalTextPosition(JLabel.CENTER);
        label10.setHorizontalTextPosition(JLabel.CENTER);
        constraints.gridx = 0;
        constraints.gridy = 4;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(label10, constraints);
        
        //televisionSeriesList
        televisionSeriesList.setSelectedIndex(1);
        constraints.gridx = 1;
        constraints.gridy = 4;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(televisionSeriesList, constraints);
        
        //button06
        constraints.gridx = 0;
        constraints.gridy = 5;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(button06, constraints);
        
        //button14
        constraints.gridx = 1;
        constraints.gridy = 5;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(button14, constraints);
        
        validate();
        repaint();
    }
    
    private void open_menu_search_genre()
    {
        previousSearchKeyword = false;
        previousSearchGenre = true;
        
        removeAll();
        
        //label11
        label11.setVerticalTextPosition(JLabel.CENTER);
        label11.setHorizontalTextPosition(JLabel.CENTER);
        label11.setFont(font01);
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(label11, constraints);
        
        //genreSearchList
        genreList2.setSelectedIndex(0);
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(genreList2, constraints);
        
        //button16
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(button16, constraints);
        
        //searchGenreList
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(searchGenreList, constraints);
        
        //button17
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(button17, constraints);
        
        //button06 - main menu
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.NONE;
        add(button06, constraints);
        
        validate();
        repaint();
    }
    
    private void open_menu_search_previous()
    {
        if(previousSearchKeyword)
            open_menu_search_keyword();
        else if(previousSearchGenre)
            open_menu_search_genre();
    }
    
    private void user_notification_media(Media m, String s)
    {
        removeAll();
        
        //notification
        JLabel notification = new JLabel(m.get_title_string() + " " + s);
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.NONE;
        add(notification, constraints);
        
        //button06 - main menu
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.NONE;
        add(button06, constraints);
        
        validate();
        repaint();
    }
    
    private void user_notification(String s)
    {
        removeAll();
        
        //notification
        JLabel notification = new JLabel(s);
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.NONE;
        add(notification, constraints);
        
        //button06 - main menu
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.NONE;
        add(button06, constraints);
        
        validate();
        repaint();
    }
    
    //------------------------------------------------------------------------//
    //                          Menu Functions                                //
    //------------------------------------------------------------------------//
    
    @SuppressWarnings("unchecked")
    private void search_keyword()
    {
        for(int i = 0; i < 3; i++)
            results_keyword[i] = new Media();
        
        //get user input        
        String[] input = textField01.getText().split(" ");
        
        //todo: check that there are keywords
        
        ArrayList<String> keywords = new ArrayList<>();
        
        int[] resultsMatchCount = {0,0,0};
        
        ArrayList<String> stringResults = new ArrayList<>();
        

        //remove common words
        for(String s : input)
        {
            //todo: check if this should be logical AND instead of OR
            if(!s.equalsIgnoreCase("a") || !s.equalsIgnoreCase("an") || !s.equalsIgnoreCase("the"))
                keywords.add(s);
        }
        
        for(Media media : database)
        {
            int matchCount = 0;
            for(String keyword : keywords)
            {
                if (media.get_title().toLowerCase().contains(keyword.toLowerCase()))
                {
                    matchCount++;
                }
            }
            
            if(matchCount > 0)
            {                
                if(matchCount > resultsMatchCount[0]) //replace 1st result
                {
                    for(int i = 2; i > 0; i--)
                    {
                        resultsMatchCount[i] = resultsMatchCount[i-1];
                        results_keyword[i] = results_keyword[i-1];
                    }
                    resultsMatchCount[0] = matchCount;
                    results_keyword[0] = media;
                }
                else if(matchCount > resultsMatchCount[1])
                {
                    for(int i = 1; i > 0; i--)
                    {
                        resultsMatchCount[i] = resultsMatchCount[i-1];
                        results_keyword[i] = results_keyword[i-1];
                    }
                    resultsMatchCount[1] = matchCount;
                    results_keyword[1] = media;
                }
                else if(matchCount > resultsMatchCount[2])
                {
                    resultsMatchCount[2] = matchCount;
                    results_keyword[2] = media;
                }
            }
        }
        
        //add media titles to string array
        for(int i = 0; i < 3; i++)
        {
            if(results_keyword[i].get_title() != null)
                stringResults.add(results_keyword[i].get_title_string());
        }
        
        searchKeywordModel.removeAllElements();
        for(String s : stringResults)
            searchKeywordModel.addElement(s);
        searchKeywordList.setModel(searchKeywordModel);
    }
    
    private void check_in(Media m)
    {
        m.set_checkedOut(false);
        m.set_checkedOutLocation("N/A");
        export_media(userdataFile.toString());
        user_notification_media(m, "has been checked in.");
    }
    
    private void check_out(Media m, String s)
    {
        m.set_checkedOut(true);
        m.set_checkedOutLocation(s);
        export_media(userdataFile.toString());
        user_notification_media(m, "has been checked out.");
    }
    
    private void delete(Media m)
    {
        for(int i = 0; i < database.size(); i++)
        {
            if(database.get(i) == m)
            {
                database.remove(m);
                break;
            }
        }
                
        search_keyword();
        export_media(userdataFile.toString());
        user_notification_media(m, "has been deleted.");
    }
    
    private void add_media()
    {
        Format format;
        Genre genre;
        boolean televisionSeries;
        
        switch(formatList.getSelectedIndex())
        {
            case 0:
                format = Format.DVD;
                break;
            case 1:
                format = Format.Bluray;
                break;
            case 2:
                format = Format.Digital;
                break;
            case 3:
                format = Format.VHS;
                break;
            default:
                format = Format.Other;
                break;
        }
        
        switch(genreList.getSelectedIndex())
        {
            case 0:
                genre = Genre.Action;
                break;
            case 1:
                genre = Genre.Comedy;
                break;
            case 2:
                genre = Genre.Drama;
                break;
            case 3:
                genre = Genre.Holiday;
                break;
            case 4:
                genre = Genre.HomeVideo;
                break;
            case 5:
                genre = Genre.Kids;
                break;
            default:
                genre = Genre.Other;
                break;
        }
        
        switch(televisionSeriesList.getSelectedIndex())
        {
            case 0:
                televisionSeries = true;
                break;
            default:
                televisionSeries = false;
                break;
        }
        
        database.add(new Media(textField03.getText(), format, genre, televisionSeries));
        export_media(userdataFile.toString());
        user_notification_media(database.get(database.size()-1), "has been added.");
    }
    
    @SuppressWarnings("unchecked")
    private void search_genre(String genre)
    {
        ArrayList<String> stringResults = new ArrayList<>(); 
        
        for(Media media : database)
        {
            if(media.get_genre().equals(genre))
            {
                stringResults.add(media.get_title_string());
            }
        }
        
        //Collections.sort(stringResults);
        
        searchGenreModel.removeAllElements();
        for(String s : stringResults)
            searchGenreModel.addElement(s);
        searchGenreList.setModel(searchGenreModel);
    }
}
